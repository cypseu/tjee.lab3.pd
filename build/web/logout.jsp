<%-- 
    Document   : logout
    Created on : 2024-02-06, 12:54:37
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core" %> 
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html" %>



<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>Logout</title>
             <link rel="stylesheet" href="Style/css/components.css">
 <link rel="stylesheet" href="Style/css/icons.css">
 <link rel="stylesheet" href="Style/css/responsee.css">
        </head>
        <body>
            <h:form id="formularz_logout">
                <h1><h:outputText value="Succesfully logout"/></h1>
                <h:commandButton value="Login again" action="login"/>
            </h:form>
        </body>
    </html>
</f:view>