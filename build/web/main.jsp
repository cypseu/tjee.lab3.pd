<%-- 
    Document   : main
    Created on : 2024-02-06, 11:38:46
    Author     : student
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core" %> 
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<f:view>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Greatest Common Divisor</title>
    </head>
    <body>
        <h1>Greatest Common Divisor</h1>
        
        <h:form id="selected">
            <p>Set number of randomized parameters:</p>
            <h:inputText value="#{GCDBean.numberOfNumbers}" />
            <h:commandButton value="Show GCD" action="#{GCDBean.generateRandomNumbers()}" />
        </h:form>


        
        
        <br/> <br/> <br/>
        <h3> Click here to logout from application </h3>
       <h:form id="logout">      
             <h:commandButton value="Log out" action="#{logowanie.logout}"/>
        </h:form>
    </body>
</html>
</f:view>
