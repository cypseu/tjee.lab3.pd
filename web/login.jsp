<%-- 
    Document   : login
    Created on : 2024-02-06, 11:25:08
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core" %> 
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<!DOCTYPE html>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>LOGIN PAGE</title>
             <link rel="stylesheet" href="Style/css/components.css">
 <link rel="stylesheet" href="Style/css/icons.css">
 <link rel="stylesheet" href="Style/css/responsee.css">
        </head>
        <body>
            <h1><h:outputText value="Log in to the application"/></h1>
            
            <h:form id="formularz_login">
                <h:outputText value="Name" /><br />
                <h:inputText value="#{logowanie.nazwa}" /><br /><br />

                <h:outputText value="Password" /><br />
                <h:inputSecret value="#{logowanie.hasło}" /><br />
                <p><i>student:wcy</i></p>
          

                <h:commandButton value="Log in" action="#{logowanie.sprawdź}" />
            </h:form>
        </body>
    </html>
</f:view>
