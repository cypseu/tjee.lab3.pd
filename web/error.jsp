<%-- 
    Document   : error
    Created on : 2024-02-06, 11:34:41
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<!DOCTYPE html>

<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>Error</title>
            <link rel="stylesheet" href="Style/css/components.css">
            <link rel="stylesheet" href="Style/css/icons.css">
            <link rel="stylesheet" href="Style/css/responsee.css">
        </head>
        
        <body>
            <h1>
                <h:outputText value="Błąd logowania"/></h1>
            <h:form id="formularz_error">
                <h:outputText value="Złe hasło, lub brak użytkownika #{login.nazwa}" /><br />
                <h:commandButton value="Spróbuj ponownie" action="try_again" />
            </h:form>  

        </body>
    </html>
</f:view>
