<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %> 
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<!DOCTYPE html>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>GCD Result</title>
        </head>
        <body>
            <h1>GCD Result</h1>
                <h:form>
                    <p>Generated random values:</p>
                    <h:outputText value="#{GCDBean.randomNumbers}" />
                    <p>Click the button to calculate GCD:</p>
                    <h:commandButton value="Calculate GCD" action="#{GCDBean.calculateGCD()}" />
                    <br/>
                    <p>Result of GCD calculation:</p>
                    <h:outputText value="#{GCDBean.result}" />
                    <br/> <br/> <br/>
                </h:form>
                
                <h:form id="back">      
                    <h:commandButton value="Go back" action="back"/>
                </h:form>
        </body>
    </html>
</f:view>
