package lab3;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@ManagedBean
@SessionScoped
public class GCDBean {

    int numberOfNumbers;
    List<Integer> randomNumbers;
    Integer result;

    public int getNumberOfNumbers() {
        return numberOfNumbers;
    }

    public void setNumberOfNumbers(int numberOfNumbers) {
        this.numberOfNumbers = numberOfNumbers;
    }

    public List<Integer> getRandomNumbers() {
        return randomNumbers;
    }

    public Integer getResult() {
        return result;
    }

    public String generateRandomNumbers() {
        randomNumbers = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < numberOfNumbers; i++) {
            randomNumbers.add(random.nextInt(100) + 1);
        }
        System.out.println(randomNumbers);
        return "selected";
    }

    public void calculateGCD() {
        if (randomNumbers != null && randomNumbers.size() > 1) {
            result = randomNumbers.get(0);
            for (int i = 1; i < randomNumbers.size(); i++) {
                result = calculateGCD(result, randomNumbers.get(i));
            }
        }else if (randomNumbers.size()==1){
            result = randomNumbers.get(0);
        }else{
            result = -1;
        }
    }

    private int calculateGCD(int a, int b) {
        while (b != 0) {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }
}
